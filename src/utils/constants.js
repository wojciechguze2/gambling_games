export const STATE_USER_STORAGE_KEY = 'user'
export const STATE_GAMES_STORAGE_KEY = 'games'
export const LINKEDIN_LINK = '#'
export const GITHUB_LINK = '#'
export const MAIL_LINK = '#'
export const PHONE_LINK = '#'
export const MOBILE_MAX_WIDTH_PX = 768