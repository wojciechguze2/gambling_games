### Instalacja (dev)
```
yarn install
yarn run start
```

### Wykorzystane technologie (frontend):
- react.js, redux, react router
- axios
- bootstrap
- fontawesome
- node.js wersja 16.16
- scss

Została dodana również biblioteka purgecss oraz react-helmet-async - pod pozycjonowanie